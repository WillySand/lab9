from django.test import TestCase,Client
from django.urls import resolve
from django.apps import apps
from .apps import JadwalConfig
class Lab9UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(JadwalConfig.name, 'Jadwal')
        self.assertEqual(apps.get_app_config('Jadwal').name, 'Jadwal')
    def test_url(self):
        response = Client().get('/hp')
        self.assertEqual(response.status_code, 301)#test existing url
        response = Client().get('/accounts/signup')
        self.assertEqual(response.status_code, 301)#test url that doesn't exist
        response = Client().get('/accounts/login')
        self.assertEqual(response.status_code, 301)
        response = Client().get('/home')
        self.assertNotEqual(response.status_code, 200)
    #def test_function(self):
#        foundFunc = resolve('/')
        #self.assertEqual(foundFunc.func, home)
