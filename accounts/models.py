from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
    skills = models.CharField(max_length=50)
